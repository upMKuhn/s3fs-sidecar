#!/bin/sh

if [ -z $S3_SPACE_NAME ]; then
    echo skipping S3 Mount
else
    mkdir -p /mnt/s3
    export DATA_DIR=/mnt/s3
    echo $S3_ACCESS_KEY:$S3_SECRET > ~/.passwd-s3fs
    chmod 400 ~/.passwd-s3fs
    echo Mounting S3 to $DATA_DIR 

    echo $S3_SPACE_NAME $DATA_DIR -f -o url=$S3_ENDPOINT,allow_other,use_cache=/tmp,max_stat_cache_size=1000,stat_cache_expire=900,retries=5,connect_timeout=10$S3_EXTRAVARS
    s3fs $S3_SPACE_NAME $DATA_DIR -f -o url=$S3_ENDPOINT,allow_other,use_cache=/tmp,max_stat_cache_size=1000,stat_cache_expire=900,retries=5,connect_timeout=10$S3_EXTRAVARS
fi

