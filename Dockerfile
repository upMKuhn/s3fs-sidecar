########################################################
# The FUSE driver needs elevated privileges, run Docker with --privileged=true 
# or with minimum elevation as shown below:
# $ sudo docker run -d --rm --name s3fs --security-opt apparmor:unconfined \
#  --cap-add mknod --cap-add sys_admin --device=/dev/fuse
########################################################

FROM alpine

ENV DATA_DIR=/mnt/s3/

ENV S3_SPACE_NAME ''
ENV S3_ACCESS_KEY ''
ENV S3_SECRET ''
ENV S3_ENDPOINT https://bucket

RUN mkdir -p $DATA_DIR

RUN apk update
RUN apk add --no-cache -X http://dl-cdn.alpinelinux.org/alpine/edge/testing s3fs-fuse dumb-init

RUN mkdir /app

ADD run.sh /app/run.sh

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD [ "/app/run.sh" ]